package edu.upc.damo.toDoList;

import android.content.Context;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Josep M on 15/10/2014.
 */
public class AdapterMeu extends ArrayAdapter<CharSequence> {
    List<CharSequence> dades = new ArrayList<CharSequence>();

    AdapterMeu(Context c, int resource, List<CharSequence> dades) {
        super(c, resource, dades);
        this.dades = dades;
        carregaDades(c);
    }

    AdapterMeu(Context c, int resource, int textResourceId, List<CharSequence> dades) {
        super(c, resource, textResourceId, dades);
        this.dades = dades;
        carregaDades(c);
    }


    private void carregaDades(Context c) {

        String[] dadesEstatiques = c.getResources().getStringArray(R.array.dadesEstatiques);

        Collections.addAll(dades, dadesEstatiques);
    }


    @Override
    public void add(CharSequence object) {
        super.add(object);
        notifyDataSetChanged();
    }


    public void del(int pos) {
        dades.remove(pos);
        notifyDataSetChanged();
    }

}
